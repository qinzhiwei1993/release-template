Changelog
### [0.3.4](https://gitlab.com/qinzhiwei1993/release-template/compare/v0.3.3...v0.3.4) (2020-12-08)


### ✨ Features | 新功能

* 测试standard-version的多次发布compare ([2cd09a4](https://gitlab.com/qinzhiwei1993/release-template/commit/2cd09a4a545fd5ec17ac7710763281411600bc80))

### 0.3.3 (2020-12-08)


### ✨ Features | 新功能

* 测试standard-version在gitlab中 ([50bcc1e](https://gitlab.com/qinzhiwei1993/release-template/commit/50bcc1edd094d47661a68149005d1d090f109595))


### 🎫 Chores | 其他更新

* 初始化 ([eb5c254](https://gitlab.com/qinzhiwei1993/release-template/commit/eb5c2541a3d9e990f75c53d5c2eb2d9f87b98096))
