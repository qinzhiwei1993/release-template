module.exports = {
    header: "Changelog",
    types: [
        { type: "feat", section: "✨ Features | 新功能" },
        { type: "fix", section: "🐛 Bug Fixes | Bug 修复" },
        { type: "chore", section: "🎫 Chores | 其他更新" },
        { type: "docs", hidden: true },
        { type: "style", hidden: true },
        { type: "refactor", hidden: true },
        { type: "perf", section: "⚡ Performance Improvements | 性能优化", hidden: true },
        { type: "test", hidden: true }
    ]
}